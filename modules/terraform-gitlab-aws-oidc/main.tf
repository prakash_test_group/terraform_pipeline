resource "aws_iam_openid_connect_provider" "default" {
  url = "https://gitlab.com"
  client_id_list = [
    "https://gitlab.com",
  ]
  # thumbprint_list = [data.tls_certificate.gitlab.certificates.0.sha1_fingerprint]
  thumbprint_list = ["b3dd7606d2b5a8b4a13771dbecc9ee1cecafa38a"]
}
resource "aws_iam_role" "gitlab_oidc_default" {
  name = "gitlab-oidc-default"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Federated": "arn:aws:iam::522820335540:oidc-provider/gitlab.com"
            },
            "Action": "sts:AssumeRoleWithWebIdentity",
            "Condition": {
                "StringLike": {
                    "gitlab.com:sub": [
                        "project_path:equilibrium-energy/*:ref_type:tag:ref:*",
                        "project_path:equilibrium-energy/*:ref_type:branch:ref:*"
                    ]
                }
            }
        }
    ]
}
EOF
}
resource "aws_iam_policy" "gitlab_oidc_default_policy" {
  name_prefix        = "gitlab-oidc-default-policy-"
  description = "A Gitlab oidc policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "eks:*"
            ],
            "Resource": "*"
        }
 
    ]
}
EOF
}
resource "aws_iam_role_policy_attachment" "oidc" {
  role       = aws_iam_role.gitlab_oidc_default.name
  policy_arn = aws_iam_policy.gitlab_oidc_default_policy.arn
}
